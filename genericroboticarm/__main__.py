#!/usr/bin/env python3
# vim:fileencoding=utf-8
"""_____________________________________________________________________

:PROJECT: genericroboticarm

* Main module command line interface *

:details:  Main module command line interface.
           !!! Warning: it should have a diffent name than the package name.

.. note:: -
________________________________________________________________________
"""
import asyncio
import argparse
import logging
from genericroboticarm import __version__

from genericroboticarm.robo_APIs.dummy_robot import DummyRobot
from genericroboticarm.control.robo_dash import RoboDashApp


logging.basicConfig(
    format="%(levelname)-4s| %(module)s.%(funcName)s: %(message)s",
    level=logging.WARNING,
)


def parse_command_line():
    """Console script for genericroboticarm."""

    description = "genericroboticarm"
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("_", nargs="*")

    parser.add_argument(
        "-r",
        "--robot",
        action="store",
        default="Dummy",
        help="Name of the robot implementation (Dummy by default). Options are: Dummy, Dorna, XArm, "
        "PF400, UppsalaXArm, JoyIt",
    )

    parser.add_argument("-a", "--address", action="store", default="127.0.0.1", help="IP address of the SiLA server")
    parser.add_argument("-p", "--port", action="store", default=50051, help="Port of the SiLA server")
    parser.add_argument(
        "-s",
        "--simulation",
        action=argparse.BooleanOptionalAction,
        default=False,
        help="Set to True to start the robot in simulation mode",
    )
    parser.add_argument(
        "-u",
        "--unitelabs",
        action=argparse.BooleanOptionalAction,
        default=False,
        help="Starts the sila interface as a unitelabs connector instead of sila_python server.",
    )

    parser.add_argument("-v", "--version", action="version", version="%(prog)s " + __version__)

    return parser.parse_args()


def main():
    """Creates and starts the connector application"""

    logging.basicConfig(format="%(levelname)-4s| %(module)s.%(funcName)s: %(message)s", level=logging.DEBUG)

    args = parse_command_line()

    # select the robot implementation
    robot = args.robot
    robo_impl = None
    if robot == "Dummy":
        robo_impl = DummyRobot()
    elif robot == "JoyIt":
         from genericroboticarm.robo_APIs.joyit.joy_it_implementation import JoyItImplementation

         if args.simulation:
             logging.warning("There is no simulation mode for the JoyIt. Press enter to continue in real mode")
             input()
         robo_impl = JoyItImplementation()
    elif robot == "Dorna":
        from genericroboticarm.robo_APIs.dorna.dorna_implementation import DornaImplementation

        robo_impl = DornaImplementation(start_simulating=args.simulation)
    elif robot == "XArm":
        from genericroboticarm.robo_APIs.xfactory import XArmImplementation

        robo_impl = XArmImplementation(start_simulating=args.simulation)
    elif robot == "UppsalaXArm":
        from genericroboticarm.robo_APIs.xfactory import UppsalaXArm

        robo_impl = UppsalaXArm(start_simulating=args.simulation)
    elif robot == "PF400":
        from genericroboticarm.robo_APIs.precise_flex import PFImplementation

        robo_impl = PFImplementation(start_simulating=args.simulation)
    else:
        print(f"What is {robot} ?")

    dash_app = RoboDashApp(robo_interface=robo_impl, port=8055)
    dash_app.run()
    if args.unitelabs:
        from .connector import create_app
        asyncio.run(create_app(robo_impl))
    else:
        from .sila_server import Server
        # start the sila sever
        server = Server(robo_interface=robo_impl)
        server.start_insecure(args.address, int(args.port))
        print("\nPress 'q' -> enter to exit.")
        while not input() == 'q':
            pass
        server.stop(grace_period=.5)


if __name__ == "__main__":
    main()
