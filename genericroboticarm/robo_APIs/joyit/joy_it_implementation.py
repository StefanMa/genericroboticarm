from __future__ import annotations

import traceback
from typing import List, Tuple, Optional, Dict
from serial import Serial
from genericroboticarm.robo_APIs.robo_interface import RoboInterface
from .joy_it_simulation import JoyItSimulation
import simplejson
import logging
import time
from math import ceil
from threading import Lock, Timer
from random import randint
from os import path
from genericroboticarm.control.graph_manager import JointState


class JoyItImplementation(RoboInterface):
    com: Serial | JoyItSimulation
    _current_position: List[float]

    def __init__(self):
        super().__init__(start_simulating=False, name="JoyIt")
        self.pos_file_name = path.join(path.dirname(__file__), "robo_pos.json")
        self._current_position = []
        self.serial_lock = Lock()
        self.pos = [0 for i in range(6)]
        self.init_connection()
        self.init_robo()

    def init_connection(self):
        try:
            self.com = Serial(port='/dev/ttyACM0', timeout=.05, baudrate=460800)
            self.pos_file_name.replace("robo_pos_sim.json", "robo_pos.json")
        except:
            print(f"Could not open serial port: {traceback.print_exc()}")
            self.pos_file_name.replace("robo_pos.json", "robo_pos_sim.json")
            self.com = JoyItSimulation()
            self.is_in_simulation_mode = True
            print("initialized simulation")

    @property
    def joint_names(self) -> list[str]:
        return ['a', 'b', 'c', 'd', 'e', 'f']

    def init_robo(self, first_try=True) -> None:
        try:
            with open(self.pos_file_name, 'r') as instream:
                last_pos = simplejson.load(instream)
                # save the position for the case communication goes wrong
                self._current_position = last_pos
                # to clear the buffer
                self.com.read_all()
                coords = {self.joint_names[i]: coord for i, coord in enumerate(last_pos)}
                self.move_to_coordinates(coords)
        except:
            #if first_try:
            #    time.sleep(1)
            #    self.init_robo(first_try=False)
            logging.error(f"Robo initialization failed: {traceback.print_exc()}")

    def send_string(self, cmd) -> str:
        with self.serial_lock:
            print(f"sending {cmd}")
            if self.com:
                self.com.write(cmd.encode("utf-8"))
                answer = self.com.read_until(b'\r').decode('utf-8').strip().strip('_')
            else:
                answer = '_'.join(str(randint(0, 90)) for i in range(6))
            print(f"received {answer}")
            return answer

    def parse_answer(self, answer: str) -> List[float]:
        parts = answer.split("_")
        coords = [float(part) for part in parts]
        self._current_position = coords
        return coords

    def save_pos(self, pos: List[float]) -> None:
        with open(self.pos_file_name, 'w') as outstream:
            simplejson.dump(pos, outstream)

    def grip_close(self):
        curr = self.current_coordinates.copy()
        # last joint named f controls the gripper
        curr['f'] = 120
        self.move_to_coordinates(curr)

    def grip_open(self):
        curr = self.current_coordinates.copy()
        # last joint named f controls the gripper
        curr['f'] = 70
        self.move_to_coordinates(curr)

    def move_straight_to_position(self, identifier: str, offset: Optional[Dict[str, float]]=None):
        if not self.halt:
            target_coord = self.graph_manager.get_coordinates(identifier)
            self.move_controlled_to(target_coord)

    def move_controlled_to(self, target: JointState):
        current = self.current_coordinates
        dist = self.graph_manager.dist(current, target)
        # round up, take one less intermediates, and then add the final position
        # the higher the precision the smoother the movement
        num_steps = ceil(dist*100/self.speed*self.precision)
        intermediates = []
        for step in range(1, num_steps):
            delta = step/num_steps
            intermediate = {
                joint_name: round(delta*target[joint_name] + (1-delta)*current[joint_name])
                # skip the last joint which controls the gripper
                for joint_name in self.joint_names[:5]}
            intermediates.append(intermediate)
        intermediates.append(target)
        for intermediate in intermediates:
            if not self.halt:
                self.move_to_coordinates(intermediate)
                time.sleep(.1/self.precision)

    def move_relative(self, movements: List[Tuple[str, float]], **kwargs):
        if not self.move_lock.locked():
            command = '_'.join(f"{move[0]}{int(move[1])}" for move in movements)
            if not self.halt:
                answer = self.send_string(command)
                self.save_pos(self.parse_answer(answer))

    def move_to_coordinates(self, coords: JointState, **kwargs) -> None:
        # do not open/close the gripper
        coords['f'] = self.current_coordinates['f']
        position = [str(int(val)) for val in coords.values()]
        command = "|" + '_'.join(position)
        if not self.halt:
            answer = self.send_string(command)
            self.save_pos(self.parse_answer(answer))

    @property
    def current_coordinates(self) -> JointState:
        return {self.joint_names[i]: coord for i, coord in enumerate(self._current_position)}

    def release(self):
        self.halt = False

    def stop_moving(self):
        self.halt = True
        # this is preliminary und will probably be removed
        Timer(interval=5, function=self.release).start()
        super().stop_moving()

