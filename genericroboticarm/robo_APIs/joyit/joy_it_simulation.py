"""

"""

joint_names = ['a', 'b', 'c', 'd', 'e', 'f']


class JoyItSimulation:
    def __init__(self):
        self.pos = [0, 0, 0, 0, 0, 0]

    def write(self, message: bytes):
        s = message.decode('utf-8')
        if s.startswith('|'):
            s = s.strip("|")
            parts = s.split('_')
            for i, part in enumerate(parts):
                self.pos[i] = int(part)
        else:
            parts = s.split('_')
            for part in parts:
                joint = part[0]
                diff = part[1:]
                idx = joint_names.index(joint)
                self.pos[idx] += int(diff)
        for i, entry in enumerate(self.pos):
            self.pos[i] = max(0, min(entry, 180))

    def read_all(self):
        return b""

    def read_until(self, expected=b"\n", size=None):
        answer = "_".join(str(i) for i in self.pos)
        return answer.encode('utf-8')
