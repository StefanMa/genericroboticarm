import logging
from typing import List, Tuple, Union
from genericroboticarm.robo_APIs.robo_interface import RoboInterface
import time
from threading import Timer
from genericroboticarm.control.graph_manager import JointState
from dorna2 import Dorna
from .dorna_simulation import DornaSim


carth_coords = "xyzab"
joint_coords = "j0j1j2j3"


class DornaImplementation(RoboInterface):
    hardware: Union[DornaSim, Dorna]

    def __init__(self, start_simulating: bool = True, name="Dorna", ip='192.168.88.21'):
        if start_simulating:
            name += "_sim"
        super().__init__(start_simulating, name)
        if start_simulating:
            self.hardware = DornaSim()
        else:
            self.hardware = Dorna()
        self.ip = ip
        # TODO maybe do this a bit later via some SiLA command to avoid starting the motors in sleeping position
        self.init_connection()

    def init_connection(self):
        if not self.hardware.connect(host=self.ip):
            logging.error(f"Failed to connect to robot on {self.ip}")
        # enables the motor
        self.hardware.set_motor(enable=1)
    @property
    def joint_names(self) -> list[str]:
        return ['x', 'y', 'z', 'a', 'b', 'j0', 'j1', 'j2', 'j3']

    def move_relative(self, movements: List[Tuple[str, float]], **kwargs):
        with self.move_lock:
            # we either to a line move or a joint move. Cartesian has priority
            is_cart_move = any(move[0] in carth_coords for move in movements)
            # convert to dictionary
            # take only elements matching the move type
            coords = {joint: int(diff) for joint, diff in movements if (joint in carth_coords) == is_cart_move}
            # rel=1 causes a relative movement
            if is_cart_move:
                self.hardware.lmove(**coords, rel=1)
            else:
                self.hardware.jmove(**coords, rel=1)

    def site_to_position_identifier(self, device: str, slot: int) -> str:
        return f"{device}_nest{slot}"

    def grip_close(self):
        with self.move_lock:
            # 7 is the duty cycle for a fully closed griper
            self.grip_to(duty=8.5)

    def grip_open(self):
        with self.move_lock:
            # 12 is the duty cycle for a fully open griper
            self.grip_to(duty=12)

    def grip_to(self, duty: float):
        self.hardware.play(cmd="pwm", pwm0=1, freq0=50, duty0=duty)
        # the gripper need some time to actually open
        time.sleep(.3)

    def move_to_coordinates(self, coords: JointState, **kwargs) -> None:
        # we use joint moves
        target = {joint: value for joint, value in coords.items() if joint in joint_coords}
        target["j4"] = coords["b"]
        # move to absolute coordinates
        self.hardware.jmove(**target, rel=0)

    @property
    def current_coordinates(self) -> JointState:
        pose = self.hardware.get_pose()
        x, y, z, a, b = pose[:5]
        state = dict(x=x, y=y, z=z, a=a, b=b)
        j0, j1, j2, j3 = self.hardware.get_all_joint()[:4]
        state.update(dict(j0=j0, j1=j1, j2=j2, j3=j3))
        return state

    def release(self):
        self.halt = False

    def stop_moving(self):
        self.halt = True
        # this is preliminary und will probably be removed
        Timer(interval=5, function=self.release).start()
        super().stop_moving()

    def __del__(self):
        self.hardware.set_motor(enable=0)
        self.hardware.close()
