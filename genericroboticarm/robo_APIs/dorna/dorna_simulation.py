import time


class DornaSim:
    def __init__(self):
        self.pos = {joint: 0 for joint in ["x", "y", "z", "a", "b", "j0", "j1", "j2", "j3"]}
        print("Created dorna simulation")

    def connect(self, **kwargs):
        print("Connecting to (simulated) hardware")

    def set_pwm(self, **kwargs):
        pass

    def set_freq(self, **kwargs):
        pass

    def set_motor(self, **kwargs):
        pass

    def play(self, **kwargs):
        self.set_duty(duty=kwargs.get("duty0", 10))

    def set_duty(self, **kwargs):
        if 'duty' in kwargs:
            print(f"setting duty to {kwargs['duty']}")
            if kwargs['duty'] == 12:
                print("Opening gripper")
            if kwargs['duty'] == 8.5:
                print("Closing gripper")

    def jmove(self, **kwargs):
        self.lmove(**kwargs)

    def lmove(self, **kwargs):
        print(f"Moving to {kwargs} ...", end='  ', flush=True)
        for joint, val in kwargs.items():
            if joint in self.pos:
                if kwargs['rel']:
                    time.sleep(.02)
                    self.pos[joint] += kwargs[joint]
                else:
                    time.sleep(.4)
                    self.pos[joint] = kwargs[joint]
        print("... done")

    def get_pose(self):
        return list(self.pos.values())[:5]

    def get_all_joint(self):
        return list(self.pos.values())[5:]



