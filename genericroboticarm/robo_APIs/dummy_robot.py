"""
An exemplary implementation of a robotic arm.
It can change its simulated location in 3d space and print logging messages.
This serves as a demo on how to implement an interface to a real robotic arm
"""
import logging
import time
from typing import List, Tuple

from .robo_interface import RoboInterface
from ..control.graph_manager import JointState


class DummyRobot(RoboInterface):
    def __init__(self):
        super().__init__(start_simulating=False, name="Dummy")
        self.pos = {name: 0 for name in self.joint_names}
        self.init_connection()

    def init_connection(self):
        logging.info("Initializing connection to hardware")

    @property
    def joint_names(self) -> list[str]:
        return ["x", "y", "z"]

    def move_relative(self, movements: List[Tuple[str, float]], **kwargs):
        for joint, diff in movements:
            self.pos[joint] += diff
        logging.info(f"Moved to {self.pos}")

    def grip_close(self):
        #logging.info("Closing Gripper")
        print("Closing Gripper")

    def grip_open(self):
        #logging.info("Opening Gripper")
        print("Opening Gripper")

    def move_to_coordinates(self, coords: JointState, **kwargs) -> None:
        time.sleep(1)
        self.pos = coords

    @property
    def current_coordinates(self) -> JointState:
        return self.pos.copy()
