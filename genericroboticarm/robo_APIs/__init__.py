from .robo_interface import RoboInterface
from .interactive_transfer import InteractiveTransfer
from abc import ABC


class InteractiveRobot(RoboInterface, InteractiveTransfer, ABC):
    """

    """


__all__=[
    "InteractiveRobot",
    "RoboInterface",
    "InteractiveTransfer",
]
