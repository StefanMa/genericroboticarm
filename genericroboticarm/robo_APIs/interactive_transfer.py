"""
An interface for robotic arm that implement the necessary utility to use the SiLA feature
LabTransferManipulatorController.
"""
from abc import ABC, abstractmethod
from typing import List, Tuple


class InteractiveTransfer(ABC):
    @property
    @abstractmethod
    def available_intermediate_actions(self) -> List[str]:
        """
        List of functionalities the arm can do together with put/get labware commands,
         like (un-)lidding, shaking reading barcode, etc.
        :return:
        """

    @property
    @abstractmethod
    def available_handover_positions(self) -> List[Tuple[str, int]]:
        """

        :return:
        """

    @property
    @abstractmethod
    def number_internal_positions(self) -> int:
        """

        :return:
        """

    @abstractmethod
    def put_labware(self, intermediate_actions: List[str], device: str, position: int):
        """
        :param position:
        :param device:
        :param intermediate_actions:
        :return:
        """

    @abstractmethod
    def get_labware(self, intermediate_actions: List[str], device: str, position: int):
        """
        :param position:
        :param device:
        :param intermediate_actions:
        :return:
        """

    @abstractmethod
    def prepare_for_output(self, internal_pos: int, device: str, position: int) -> bool:
        """

        :param position:
        :param device:
        :param internal_pos:
        :return:
        """

    @abstractmethod
    def prepare_for_input(self, internal_pos: int, device: str, position: int, plate_type: str) -> bool:
        """

        :param plate_type:
        :param plate_type:
        :param position:
        :param device:
        :param internal_pos:
        :return:
        """

    @abstractmethod
    def cancel_transfer(self):
        """

        :return:
        """

