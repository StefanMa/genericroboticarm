import time
from typing import List, Tuple, Union
from genericroboticarm.robo_APIs.robo_interface import RoboInterface
from threading import Timer
from math import dist as euclidean_dist
from genericroboticarm.control.graph_manager import JointState
from xarm import XArmAPI
from .xarm_simulation import XArmSimulation


class XArmImplementation(RoboInterface):
    hardware: Union[XArmSimulation, XArmAPI]

    def __init__(self, start_simulating: bool = True, name="XArm", ip='192.168.1.203'):
        super().__init__(start_simulating, name)
        self.speed = 100
        self.gripper_limit = 100  # how much the gripper opens in percentage
        if start_simulating:
            self.hardware = XArmSimulation()
        else:
            self.hardware = XArmAPI(ip)
        self.ip = ip
        # TODO maybe do this a bit later via some SiLA command to avoid starting the motors in sleeping position
        self.init_connection()
        self.gripper_occupied = False
        self.set_metrik(self.dist)

    def init_connection(self):
        self.hardware.set_bio_gripper_enable()
        self.hardware.motion_enable()
        self.hardware.set_state(0)
        # it opens the grip automatically on startups
        #self.grip_open()

    def dist(self, u: JointState, v: JointState) -> float:
        u_coordinates = list(u.values())
        v_coordinates = list(v.values())
        for i in range(3, 6):
            for coordinates in [v_coordinates, u_coordinates]:
                # for circular joints -150 equals 210 and especially -179 is near 179
                coordinates[i] = (coordinates[i] + 360) % 360
        return euclidean_dist(u_coordinates, v_coordinates)

    @property
    def joint_names(self) -> list[str]:
        return ['x', 'y', 'z', 'roll', 'pitch', 'yaw']

    def move_relative(self, movements: List[Tuple[str, float]], **kwargs):
        with self.move_lock:
            # fill the unused directions with 0
            coordinates = {joint: 0. for joint in self.joint_names}
            for joint, diff in movements:
                # this makes the set speed also affect the manual movement speed
                factor = self.speed/100
                coordinates[joint] = factor * diff
            self.hardware.set_position(**coordinates, relative=True, wait=True, speed=self.speed)

    def grip_close(self):
        print("closing gripper. status: ", self.hardware.get_bio_gripper_status()[1])
        with self.move_lock:
            if self.hardware.get_bio_gripper_status()[1] == 9:
                self.hardware.close_bio_gripper(wait=False)
            self.hardware.set_bio_gripper_enable()
            self.hardware.close_bio_gripper(wait=True)

    def grip_open(self):
        print("opening gripper. status: ", self.hardware.get_bio_gripper_status()[1])
        with self.move_lock:
            if self.gripper_limit < 100:
                self._grip_half_open()
            else:
                self.hardware.open_bio_gripper(wait=True)

    def _grip_half_open(self, interval: float = .7):
        self.hardware.open_bio_gripper(speed=1, wait=False)
        time.sleep(interval)
        self.hardware.set_bio_gripper_enable(False)
        #Timer(function=self.hardware.set_bio_gripper_enable, args=[False], interval=interval).start()

    def move_to_coordinates(self, coords: JointState, **kwargs) -> None:
        # move to absolute coordinates
        self.hardware.set_position(**coords, relative=False, wait=True, speed=self.speed)

    @property
    def current_coordinates(self) -> JointState:
        pose = self.hardware.get_position()
        x, y, z, r, p, yaw = pose[1]
        state = dict(x=x, y=y, z=z, roll=r, pitch=p, yaw=yaw)
        return state

    def release(self):
        self.halt = False
        self.init_connection()

    def stop_moving(self):
        self.halt = True
        self.hardware.emergency_stop()
        # release again after 5 seconds
        Timer(function=self.release, interval=5).start()
        super().stop_moving()

    def end_connection(self):
        print("disconnecting")
        super().end_connection()
        self.hardware.disconnect()
