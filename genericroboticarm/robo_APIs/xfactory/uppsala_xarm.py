import traceback
from typing import Optional, Dict, List, Tuple
import logging
from genericroboticarm.robo_APIs.xfactory import XArmImplementation
from genericroboticarm.robo_APIs import InteractiveTransfer


def approach_position(device: str) -> str:
    approach_pos = f"{device}_approach"
    logging.debug(f"Approach position to device {device} is {approach_pos}.")
    return approach_pos


def device_from_identifier(identifier: str) -> str:
    device_name = identifier.split('_')[0]
    logging.debug(f"{identifier} belongs to device {device_name}.")
    return device_name


class UppsalaXArm(XArmImplementation, InteractiveTransfer):
    def move_to_safe_pos(self, identifier: str):
        """
        Moves to the approach position belonging to the specified position
        :param identifier:
        :return:
        """
        device_name = device_from_identifier(identifier)
        safe_pos = approach_position(device_name)
        self.move_to_position(safe_pos)

    def pick_at_position(self, identifier: str, offset: Optional[Dict[str, float]]=None):
        if "Washer" in identifier:
            self.grip_close()
            self.gripper_limit = 50
            self.grip_open()
        super().pick_at_position(identifier, offset)
        self.move_to_safe_pos(identifier)
        if "Washer" in identifier:
            self.gripper_limit = 100

    def place_at_position(self, identifier: str, offset: Optional[Dict[str, float]]=None):
        if "Washer" in identifier:
            self.gripper_limit = 50
        super().place_at_position(identifier, offset)
        self.move_to_safe_pos(identifier)
        if "Washer" in identifier:
            self.gripper_limit = 100
            print("Enabeling gripper.")
            self.grip_close()
            self.grip_open()

    def site_to_position_identifier(self, device: str, slot: int) -> str:
        if device == "Cytomat2C" and slot > 0:
            print(f"Moving to slot{slot} means placing at nest0.")
            slot = 0
        position_name = f"{device}_nest{slot}"
        return position_name

    # Methods for standardised labware transfer

    @property
    def available_intermediate_actions(self) -> List[str]:
        return ["unlid", "lid", "read_barcode"]

    def read_barcode(self):
        self.move_to_position("BC_reader")

    @property
    def available_handover_positions(self) -> List[Tuple[str, int]]:
        return []

    @property
    def number_internal_positions(self) -> int:
        return 1

    def put_labware(self, intermediate_actions: List[str], device: str, position: int):
        position_identifier = self.site_to_position_identifier(device, position - 1)
        self.place_at_position(position_identifier)

    def get_labware(self, intermediate_actions: List[str], device: str, position: int):
        position_identifier = self.site_to_position_identifier(device, position - 1)
        self.pick_at_position(position_identifier)
        if intermediate_actions:
            print(f"We have intermediate actions: {intermediate_actions}")
            if "read_barcode" in intermediate_actions:
                self.read_barcode()

    def prepare_for_output(self, internal_pos: int, device: str, position: int) -> bool:
        try:
            safe_pos = approach_position(device)
            self.move_to_position(safe_pos)
        except Exception:
            logging.error(traceback.print_exc())
            return False
        return True

    def prepare_for_input(self, internal_pos: int, device: str, position: int, plate_type: str) -> bool:
        try:
            safe_pos = approach_position(device)
            self.move_to_position(safe_pos)
            self.grip_open()
        except Exception:
            logging.error(traceback.print_exc())
            return False
        return True

    def cancel_transfer(self):
        pass

