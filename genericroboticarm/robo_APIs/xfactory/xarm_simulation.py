from time import sleep


class XArmSimulation:
    def __init__(self):
        self.position = dict(x=0, y=0, z=0, roll=0, pitch=0, yaw=0)

    def set_bio_gripper_enable(self, ):
        sleep(0)

    def motion_enable(self, ):
        sleep(0)

    def set_state(self, new_state: int):
        sleep(0)

    def set_position(self, relative: bool, **kwargs):
        sleep(1)
        if relative:
            for k, v in kwargs.items():
                if k in self.position:
                    self.position[k] += v
        else:
            self.position.update(kwargs)

    def close_bio_gripper(self, **kwargs):
        sleep(.5)

    def open_bio_gripper(self, **kwargs):
        sleep(.5)

    def get_bio_gripper_status(self, **kwargs):
        return (0, 8)

    def get_position(self, **kwargs):
        return 0, [self.position[joint] for joint in ['x', 'y', 'z', 'roll', 'pitch', 'yaw']]

    def emergency_stop(self, **kwargs):
        sleep(0)

    def disconnect(self, **kwargs):
        sleep(0)


