from genericroboticarm.connector.features import RobotTeachingServiceBase
from genericroboticarm.robo_APIs import InteractiveRobot


class RobotTeachingService(RobotTeachingServiceBase):
    async def move_relative(self, movements: list[list[str, float]]):
        print(movements)
        #todo this seems to cause problems wit sila_unitelabs
        self.robot.move_relative(movements)

    def __init__(self, robot: InteractiveRobot):
        super().__init__()
        self.robot = robot

    async def add_position(self, identifier: str):
        self.robot.graph_manager.add_position(identifier=identifier, pos=self.robot.current_coordinates)

    async def add_connection(self, tail: str, head: str):
        self.robot.graph_manager.add_connection(tail, head)

    async def reteach_position(self, identifier: str):
        self.robot.graph_manager.change_position(identifier=identifier, pos=self.robot.current_coordinates)

    async def add_intermediates(self, a: str, b: str, number, name_generator):
        self.robot.graph_manager.add_intermediates(
            from_pos=a, to_pos=b, num=number, name_generator=lambda i: eval('f"' + name_generator + '"')
        )

    async def remove_position(self, identifier: str):
        self.robot.graph_manager.remove_position(identifier=identifier)

    async def remove_connection(self, tail: str, head: str):
        self.robot.graph_manager.remove_connection(tail, head)
