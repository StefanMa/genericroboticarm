import typing

from unitelabs.cdk import sila

from genericroboticarm.connector.features import LabwareTransferManipulatorControllerBase
from genericroboticarm.connector.features.labware_transfer_manipulator_controller_base import (
    HandoverPosition,
    PositionIndex,
    InvalidCommandSequence,
)
from genericroboticarm.robo_APIs import InteractiveRobot


class LabwareTransferManipulatorController(LabwareTransferManipulatorControllerBase):
    def __init__(self, robot: InteractiveRobot):
        super().__init__()
        self.robot = robot

    async def prepare_for_input(
        self,
        handover_position: HandoverPosition,
        internal_position: PositionIndex,
        labware_type: str,
        labware_unique_id: str,
        *,
        status: sila.Status
    ) -> None:
        preparation_succeeded = self.robot.prepare_for_input(
            internal_pos=internal_position.position_index,
            device=handover_position.position,
            position=handover_position.sub_position.position_index,
            plate_type=labware_type,
        )
        if not preparation_succeeded:
            self.robot.cancel_transfer()
            raise InvalidCommandSequence("Robot failed to prepare for input.")

    async def prepare_for_output(
        self, handover_position: HandoverPosition, internal_position: PositionIndex, *, status: sila.Status
    ) -> None:
        preparation_succeeded = self.robot.prepare_for_output(
            internal_pos=internal_position.position_index,
            device=handover_position.position,
            position=handover_position.sub_position.position_index,
        )
        if not preparation_succeeded:
            self.robot.cancel_transfer()
            raise InvalidCommandSequence("Robot failed to prepare for output.")

    async def put_labware(
        self, handover_position: HandoverPosition, intermediate_actions: list[str], *, status: sila.Status
    ) -> None:
        self.robot.put_labware(
            intermediate_actions=intermediate_actions,
            device=handover_position.position,
            position=handover_position.sub_position.position_index,
        )

    async def get_labware(
        self, handover_position: HandoverPosition, intermediate_actions: list[str], *, status: sila.Status
    ) -> None:
        self.robot.get_labware(
            intermediate_actions=intermediate_actions,
            device=handover_position.position,
            position=handover_position.sub_position.position_index,
        )

    async def get_available_handover_positions(self) -> list[HandoverPosition]:
        # convert to the custom datatype
        positions = [HandoverPosition(*hp) for hp in self.robot.available_handover_positions]
        return positions

    async def get_number_of_internal_positions(
        self,
    ) -> typing.Annotated[int, sila.constraints.MinimalInclusive(value=1)]:
        n = self.robot.number_internal_positions
        return n

    async def get_available_intermediate_actions(
        self,
    ) -> list[
        typing.Annotated[
            str, sila.constraints.FullyQualifiedIdentifier(value=sila.constraints.Identifier.COMMAND_IDENTIFIER)
        ]
    ]:
        available = self.robot.available_intermediate_actions
        print(available)
        # TODO these are not really fully qualified identifiers
        return available
