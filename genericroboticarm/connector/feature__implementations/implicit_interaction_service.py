import logging

from genericroboticarm.connector.features import ImplicitInteractionServiceBase
from genericroboticarm.control.device_interaction import DeviceList


class ImplicitInteractionService(ImplicitInteractionServiceBase):

    def __init__(self, device_list: DeviceList):
        super().__init__()
        self.devices = device_list

    async def add_device(self, server_name: str) -> None:
        self.devices.add_device(server_name)

    async def remove_device(self, server_name: str) -> None:
        self.devices.remove_device(server_name)

    async def connect_to_device(self, server_name: str, timeout: float) -> bool:
        client = self.devices.get_client(server_name, timeout)
        if client:
            return True
        else:
            logging.error(f"Failed to connect to {server_name}")
            return False

    async def current_device_set(self) -> list[str]:
        return self.devices.devices
