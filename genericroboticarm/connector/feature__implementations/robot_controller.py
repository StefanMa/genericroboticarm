from unitelabs.cdk import sila

from genericroboticarm.connector.features import RobotControllerBase
from genericroboticarm.connector.features.robot_controller_base import Site
from genericroboticarm.robo_APIs import InteractiveRobot


class RobotController(RobotControllerBase):

    def __init__(self, robot: InteractiveRobot):
        super().__init__()
        self.robot = robot

    async def reinitialize(self):
        self.robot.init_connection()

    async def set_speed(self, percentage: float):
        self.robot.set_speed(percentage)

    async def set_acceleration(self, percentage: float):
        self.robot.set_acceleration(percentage)

    async def emergency_stop(self):
        self.robot.stop_moving()

    async def move_to_position(
        self,
        position: str,
        *,
        status: sila.Status,
    ) -> None:
        """Moves to a specified position in the position graph along the edges."""
        self.robot.move_to_position(position)

    async def pick_plate(self, site: Site, *, status: sila.Status):
        self.robot.pick_from_device(site.device, site.site_index)

    async def place_plate(self, site: Site, *, status: sila.Status):
        self.robot.place_at_device(site.device, site.site_index)

    async def move_plate(
        self, origin_site: Site, target_site: Site, additional_actions: list[str], *, status: sila.Status
    ):
        self.robot.pick_from_device(origin_site.device, origin_site.site_index)
        self.robot.place_at_device(target_site.device, target_site.site_index)

    async def custom_command(self, command: str) -> str:
        return self.robot.run_custom_command(command)
