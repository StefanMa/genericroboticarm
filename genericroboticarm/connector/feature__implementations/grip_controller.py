from genericroboticarm.connector.features import GripControllerBase
from genericroboticarm.robo_APIs import InteractiveRobot


class GripController(GripControllerBase):
    def __init__(self, robot: InteractiveRobot):
        super().__init__()
        self.robot = robot

    async def grip(self) -> None:
        self.robot.grip_close()

    async def release(self) -> None:
        self.robot.grip_open()
