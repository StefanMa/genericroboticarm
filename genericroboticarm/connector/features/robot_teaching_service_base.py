import abc
from unitelabs.cdk import sila


class RobotTeachingServiceBase(sila.Feature, metaclass=abc.ABCMeta):
    """
    Feature to teach a robotic arm a graph consisting of positions in its own coordinate system as well as
    connections between them. A connection between two position means, the robot is allowed to move along a straight
    (in its own coordinate system) line between those positions.
    """

    def __init__(self):
        super().__init__(
            originator="io.pharmb",
            category="robot",
            version="1.0",
            maturity_level="Draft",
        )

    @abc.abstractmethod
    @sila.UnobservableCommand()
    async def move_relative(self, movements: list[list[str, float]]):
        """
        Moves the robot relative to its current position.

        .. parameter::  The movements given as a list of tuples (joint name, movement).
        """

    @abc.abstractmethod
    @sila.UnobservableCommand()
    async def add_position(self, identifier: str):
        """
        Adds the current position to the movement graph.

        .. parameter:: Name of the new position.
        """

    @abc.abstractmethod
    @sila.UnobservableCommand()
    async def add_connection(self, tail: str, head: str):
        """
        Adds a connection between two known positions to the movement graph.

        .. parameter:: Name of the tail of the edge.
        .. parameter:: Name of the head of the edge.
        """

    @abc.abstractmethod
    @sila.UnobservableCommand()
    async def reteach_position(self, identifier: str):
        """
        Reteaches the current position in the movement graph to the current robot position.

        .. parameter:: Name of the new position.
        """

    @abc.abstractmethod
    @sila.UnobservableCommand()
    async def add_intermediates(self, a: str, b: str, number: int, name_generator: str):
        """
        Add a number of equidistant positions A and B on a line between two positions.

        .. parameter:: Identifier of position A.
        .. parameter:: Identifier of position B.
        .. parameter:: Number of intermediate positions.
        .. parameter:: specify a python name generator (without the leading 'f' or " or ' around) that receives the number of the
                intermediate position as parameter i starting with 0 for the intermediate position nearest to A.
                For example intermediate_{i+1}.
        """

    @abc.abstractmethod
    @sila.UnobservableCommand()
    async def remove_position(self, identifier: str):
        """
        Removes the current position to the movement graph.

        .. parameter:: Name of the new position.
        """

    @abc.abstractmethod
    @sila.UnobservableCommand()
    async def remove_connection(self, tail: str, head: str):
        """
        Removes a connection between two known positions to the movement graph.

        .. parameter:: Name of the tail of the edge.
        .. parameter:: Name of the head of the edge.
        """
