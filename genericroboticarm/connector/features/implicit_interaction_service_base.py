import abc

from unitelabs.cdk import sila


class ImplicitInteractionServiceBase(sila.Feature, metaclass=abc.ABCMeta):
    """
    Feature to add implicit interaction with other devices for labware transfer.
    This feature allows to manage a set sila connectors specified by their sila server_name which must implement
    the LabwareTransferSiteController feature. When a MovePlate PickPlate or PlacePlate command from the RobotController
    feature of this connector is called and the specified site (, i.e., the parameter Site.device) is in the set of
    for implicit interaction the corresponding methods (PrepareForInput, PrepareForOutput, LabwarePlaced and LabwareRemoved)
    are automatically called before/after picking/placing labware.
    The robot automatically connects to listed devices via SiLA discovery and will not move labware to/from
    a listed device if so connection can be established.
    """

    def __init__(self):
        super().__init__(
            originator="io.pharmb",
            category="robot",
            version="1.0",
            maturity_level="Draft",
        )

    @abc.abstractmethod
    @sila.UnobservableCommand()
    async def add_device(self, server_name: str) -> None:
        """
        Adds a device to the set.

        .. parameter:: The SilaService.server_name of the device.
        """

    @abc.abstractmethod
    @sila.UnobservableCommand()
    async def remove_device(self, server_name: str) -> None:
        """
        Removes a device to the set.

        .. parameter:: The SilaService.server_name of the device.
        """

    @abc.abstractmethod
    @sila.UnobservableCommand()
    @sila.Response(name="Success", description="Boolean whether connection was established")
    async def connect_to_device(self, server_name: str, timeout: float) -> bool:
        """
        Makes the robot try to establish a connection to the specified device(this might take a few seconds).
        Doing this before the connection is actually needed may save time.
        Returns whether the connection could be established.

        .. parameter:: SilaService.ServerName of the device connector
        .. parameter:: Timeout for connection establishment
        """

    @abc.abstractmethod
    @sila.UnobservableProperty()
    async def current_device_set(self) -> list[str]:
        """
        A list of all devices currently set for implicit interaction.
        """
