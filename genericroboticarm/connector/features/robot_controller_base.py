import abc
from unitelabs.cdk import sila
import dataclasses


@dataclasses.dataclass
class Site(sila.CustomDataType):
    """
    Specifies a site on a robotic platform.
    It consists of the name of the device and the position index within that device.

    .. parameter:: The device name.
    .. parameter:: The index of the site position.
    """

    device: str
    site_index: int


class RobotControllerBase(sila.Feature, metaclass=abc.ABCMeta):
    """
    Controls a robotic arm.
    """

    def __init__(self):
        super().__init__(
            originator="io.pharmb",
            category="robot",
            version="1.0",
            maturity_level="Draft",
        )

    @abc.abstractmethod
    @sila.UnobservableCommand()
    async def reinitialize(self):
        """
        Clears all errors and re-initializes the connection and the robot itself.
        """

    @abc.abstractmethod
    @sila.UnobservableCommand()
    async def set_speed(self, percentage: float):
        """
        Sets the speed.

        .. parameter:: A vale between 0 and 100.
        """

    @abc.abstractmethod
    @sila.UnobservableCommand()
    async def set_acceleration(self, percentage: float):
        """
        Sets the acceleration.

        .. parameter:: A vale between 0 and 100.
        """

    @abc.abstractmethod
    @sila.UnobservableCommand()
    async def emergency_stop(self):
        """
        Stops the robot as quickly as possible.
        """

    @abc.abstractmethod
    @sila.ObservableCommand()
    async def move_to_position(self, position: str, *, status: sila.Status) -> None:
        """
        The abstract method to move to the position.

        .. parameter:: Identifier of the position.
        .. yield:: The progress of the method execution in percent.
        """

    @abc.abstractmethod
    @sila.ObservableCommand()
    async def pick_plate(self, site: Site, *, status: sila.Status):
        """
        Places a plate on a specified site.

        .. parameter:: The site where to pick the plate.
        .. yield:: The progress of the method execution in percent.
        """

    @abc.abstractmethod
    @sila.ObservableCommand()
    async def place_plate(self, site: Site, *, status: sila.Status):
        """
        Picks a plate from a specified site.

        .. parameter:: The site where to place the plate.
        .. yield:: The progress of the method execution in percent.
        """

    @abc.abstractmethod
    @sila.ObservableCommand()
    async def move_plate(
        self, origin_site: Site, target_site: Site, additional_actions: list[str], *, status: sila.Status
    ):
        """
        Moves a plate from one site to another.

        .. parameter:: Where to pick the plate.
        .. parameter:: Where to place the plate.
        .. parameter:: List of intermediate actions like barcode reading, lidding, etc.
        .. yield:: The progress of the method execution in percent.
        """

    @abc.abstractmethod
    @sila.UnobservableCommand()
    @sila.Response(name="Output", description="The output as a string")
    async def custom_command(self, command: str) -> str:
        """
        Runs a custom command. Implementation of this can vary between robots.
        If not implemented for this robot 'Not Implemented' is returned.

        .. parameter:: The command
        """
