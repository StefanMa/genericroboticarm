from unitelabs.cdk import Connector
from genericroboticarm.robo_APIs import InteractiveRobot

from .feature__implementations import (
    GripController,
    RobotController,
    RobotTeachingService,
    LabwareTransferManipulatorController,
    ImplicitInteractionService,
)


async def create_app(robot: InteractiveRobot):
    """Creates the connector application"""
    app = Connector(
        {
            "sila_server": {
                "name": "Generic Robot Arm",
                "type": "Robot",
                "description": "A Generic Implementation for a robotic arm",
                "version": "0.1.0",
                "vendor_url": "uu.se",
            }
        }
    )

    app.register(GripController(robot))
    app.register(RobotController(robot))
    app.register(RobotTeachingService(robot))

    # check whether the robot implements the labware transfer interaction
    #if isinstance(robot, InteractiveTransfer):
    app.register(LabwareTransferManipulatorController(robot))
    app.register(ImplicitInteractionService(robot.interacting_devices))

    await app.start()

    return app
