graph [
  node [
    id 0
    label "P1"
    x -2
    y 0
    z 0
  ]
  node [
    id 1
    label "P2"
    x -2
    y 8
    z 0
  ]
  node [
    id 2
    label "P3"
    x -2
    y 12
    z 35
  ]
  node [
    id 3
    label "P4"
    x 24
    y 8
    z 42
  ]
  node [
    id 4
    label "P5"
    x 24
    y 38
    z 42
  ]
  node [
    id 5
    label "P6"
    x 9
    y 38
    z 42
  ]
  node [
    id 6
    label "P7"
    x 2
    y 38
    z 42
  ]
  node [
    id 7
    label "New"
    x 34
    y 30
    z 12
  ]
  node [
    id 8
    label "New2"
    x 33
    y 46
    z 14
  ]
  edge [
    source 0
    target 1
    dist 8.0
  ]
  edge [
    source 0
    target 2
    dist 35.90264614203248
  ]
  edge [
    source 0
    target 4
    dist 62.321745803531535
  ]
  edge [
    source 0
    target 7
    dist 48.373546489791295
  ]
  edge [
    source 2
    target 3
    dist 26.92582403567252
  ]
  edge [
    source 2
    target 6
    dist 31.064449134018133
  ]
  edge [
    source 3
    target 4
    dist 30.0
  ]
  edge [
    source 3
    target 5
    dist 33.54101966249684
  ]
  edge [
    source 4
    target 8
    dist 30.479501308256342
  ]
  edge [
    source 5
    target 6
    dist 7.0
  ]
]
