graph [
  node [
    id 0
    label "Home"
    x 0
    y 0
    z 0
    a 0
    b 0
  ]
  node [
    id 1
    label "Active"
    x 67
    y -2
    z -3
    a 0
    b 0
  ]
  node [
    id 2
    label "Flow_safe"
    x 29
    y 1
    z 12
    a 12
    b 0
  ]
  node [
    id 3
    label "Flow_Slot1_above"
    x 12
    y 1
    z 12
    a 12
    b 4
  ]
  node [
    id 4
    label "Flow_Slot2_above"
    x 12
    y 15
    z 12
    a 12
    b 4
  ]
  node [
    id 5
    label "Flow_Slot3_above"
    x 12
    y 28
    z 12
    a 12
    b 4
  ]
  edge [
    source 0
    target 1
    dist 67.09694478886502
  ]
  edge [
    source 1
    target 2
    dist 42.68489194082609
  ]
  edge [
    source 2
    target 3
    dist 17.46424919657298
  ]
  edge [
    source 2
    target 4
    dist 22.38302928559939
  ]
  edge [
    source 2
    target 5
    dist 32.155870381627054
  ]
]
