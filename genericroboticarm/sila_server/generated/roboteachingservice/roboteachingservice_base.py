# Generated by sila2.code_generator; sila2.__version__: 0.12.2
from __future__ import annotations

from abc import ABC, abstractmethod
from typing import TYPE_CHECKING, Any, List

from sila2.server import FeatureImplementationBase, MetadataDict

from .roboteachingservice_types import (
    AddConnection_Responses,
    AddIntermediates_Responses,
    AddPosition_Responses,
    GripClose_Responses,
    GripOpen_Responses,
    MoveRelative_Responses,
    RemoveConnection_Responses,
    RemovePosition_Responses,
    ReteachPosition_Responses,
)

if TYPE_CHECKING:

    from ...server import Server


class RoboTeachingServiceBase(FeatureImplementationBase, ABC):
    parent_server: Server

    def __init__(self, parent_server: Server):
        """
        Provides a command for individual movements of joints and memorization of positions and paths
        """
        super().__init__(parent_server=parent_server)

    @abstractmethod
    def get_CurrentGraph(self, *, metadata: MetadataDict) -> str:
        """
        Returns the current graph as networkx string

        :param metadata: The SiLA Client Metadata attached to the call
        :return: Returns the current graph as networkx string
        """

    @abstractmethod
    def MoveRelative(self, Movements: List[Any], *, metadata: MetadataDict) -> MoveRelative_Responses:
        """
        Moves the robotic arms joints by specified amounts


        :param Movements: List of movements of individual joints

        :param metadata: The SiLA Client Metadata attached to the call

        """

    @abstractmethod
    def AddPosition(self, PositionIdentifier: str, *, metadata: MetadataDict) -> AddPosition_Responses:
        """
        Adds the current position to the movement graph


        :param PositionIdentifier: Name of the new position

        :param metadata: The SiLA Client Metadata attached to the call

        """

    @abstractmethod
    def AddIntermediates(
        self, A: str, B: str, Number: int, NameGenerator: str, *, metadata: MetadataDict
    ) -> AddIntermediates_Responses:
        """
        Add a number of equidistant positions A and B on a line between two positions.


        :param A: Identifier of position A

        :param B: Identifier of position B

        :param Number: Number of intermediate positions

        :param NameGenerator: Specify a python name generator (without the leading 'f' or " or ' around) that receives the number of the
                intermediate position as parameter i starting with 0 for the intermediate position nearest to A.
                For example intermediate_{i+1}.

        :param metadata: The SiLA Client Metadata attached to the call

        """

    @abstractmethod
    def ReteachPosition(self, PositionIdentifier: str, *, metadata: MetadataDict) -> ReteachPosition_Responses:
        """
        Reteaches the current position in the movement graph to the current robot position.


        :param PositionIdentifier: Name of the new position

        :param metadata: The SiLA Client Metadata attached to the call

        """

    @abstractmethod
    def AddConnection(self, Tail: str, Head: str, *, metadata: MetadataDict) -> AddConnection_Responses:
        """
        Adds a connection between two known positions to the movement graph


        :param Tail: Tail of the edge

        :param Head: Head of the edge

        :param metadata: The SiLA Client Metadata attached to the call

        """

    @abstractmethod
    def RemovePosition(self, PositionIdentifier: str, *, metadata: MetadataDict) -> RemovePosition_Responses:
        """
        Removes the current position to the graph


        :param PositionIdentifier: Name of the new Position

        :param metadata: The SiLA Client Metadata attached to the call

        """

    @abstractmethod
    def RemoveConnection(self, Tail: str, Head: str, *, metadata: MetadataDict) -> RemoveConnection_Responses:
        """
        Forbids the robot to move straight between two positions.


        :param Tail: Tail of the edge

        :param Head: Head of the edge

        :param metadata: The SiLA Client Metadata attached to the call

        """

    @abstractmethod
    def GripClose(self, *, metadata: MetadataDict) -> GripClose_Responses:
        """
        Closes the gripper


        :param metadata: The SiLA Client Metadata attached to the call

        """

    @abstractmethod
    def GripOpen(self, *, metadata: MetadataDict) -> GripOpen_Responses:
        """
        Opens the gripper


        :param metadata: The SiLA Client Metadata attached to the call

        """
