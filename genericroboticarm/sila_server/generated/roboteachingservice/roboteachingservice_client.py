# Generated by sila2.code_generator; sila2.__version__: 0.12.2
# -----
# This class does not do anything useful at runtime. Its only purpose is to provide type annotations.
# Since sphinx does not support .pyi files (yet?), this is a .py file.
# -----

from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:

    from typing import Any, Iterable, List, Optional

    from roboteachingservice_types import (
        AddConnection_Responses,
        AddIntermediates_Responses,
        AddPosition_Responses,
        GripClose_Responses,
        GripOpen_Responses,
        MoveRelative_Responses,
        RemoveConnection_Responses,
        RemovePosition_Responses,
        ReteachPosition_Responses,
    )
    from sila2.client import ClientMetadataInstance, ClientUnobservableProperty


class RoboTeachingServiceClient:
    """
    Provides a command for individual movements of joints and memorization of positions and paths
    """

    CurrentGraph: ClientUnobservableProperty[str]
    """
    Returns the current graph as networkx string
    """

    def MoveRelative(
        self, Movements: List[Any], *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> MoveRelative_Responses:
        """
        Moves the robotic arms joints by specified amounts
        """
        ...

    def AddPosition(
        self, PositionIdentifier: str, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> AddPosition_Responses:
        """
        Adds the current position to the movement graph
        """
        ...

    def AddIntermediates(
        self,
        A: str,
        B: str,
        Number: int,
        NameGenerator: str,
        *,
        metadata: Optional[Iterable[ClientMetadataInstance]] = None,
    ) -> AddIntermediates_Responses:
        """
        Add a number of equidistant positions A and B on a line between two positions.
        """
        ...

    def ReteachPosition(
        self, PositionIdentifier: str, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> ReteachPosition_Responses:
        """
        Reteaches the current position in the movement graph to the current robot position.
        """
        ...

    def AddConnection(
        self, Tail: str, Head: str, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> AddConnection_Responses:
        """
        Adds a connection between two known positions to the movement graph
        """
        ...

    def RemovePosition(
        self, PositionIdentifier: str, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> RemovePosition_Responses:
        """
        Removes the current position to the graph
        """
        ...

    def RemoveConnection(
        self, Tail: str, Head: str, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> RemoveConnection_Responses:
        """
        Forbids the robot to move straight between two positions.
        """
        ...

    def GripClose(self, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None) -> GripClose_Responses:
        """
        Closes the gripper
        """
        ...

    def GripOpen(self, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None) -> GripOpen_Responses:
        """
        Opens the gripper
        """
        ...
