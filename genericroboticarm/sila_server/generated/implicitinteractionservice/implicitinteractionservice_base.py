# Generated by sila2.code_generator; sila2.__version__: 0.12.2
from __future__ import annotations

from abc import ABC, abstractmethod
from typing import TYPE_CHECKING, List

from sila2.server import FeatureImplementationBase, MetadataDict

from .implicitinteractionservice_types import AddDevice_Responses, ConnectToDevice_Responses, RemoveDevice_Responses

if TYPE_CHECKING:

    from ...server import Server


class ImplicitInteractionServiceBase(FeatureImplementationBase, ABC):
    parent_server: Server

    def __init__(self, parent_server: Server):
        """
        Feature to add implicit interaction with other devices for labware transfer. This feature allows to
        manage a set sila connectors specified by their sila server_name which must implement the
        LabwareTransferSiteController feature. When a MovePlate PickPlate or PlacePlate command from the RobotController
        feature of this connector is called and the specified site (, i.e., the parameter Site.device) is in the set of
        for implicit interaction the corresponding methods (PrepareForInput, PrepareForOutput, LabwarePlaced and
        LabwareRemoved) are automatically called before/after picking/placing labware. The robot automatically connects
        to listed devices via SiLA discovery and will not move labware to/from a listed device if so connection can be
        established.
        """
        super().__init__(parent_server=parent_server)

    @abstractmethod
    def get_CurrentDeviceSet(self, *, metadata: MetadataDict) -> List[str]:
        """
        A list of all devices currently set for implicit interaction.

        :param metadata: The SiLA Client Metadata attached to the call
        :return: A list of all devices currently set for implicit interaction.
        """

    @abstractmethod
    def AddDevice(self, ServerName: str, *, metadata: MetadataDict) -> AddDevice_Responses:
        """
        Adds a device to the set.


        :param ServerName: The SilaService.server_name of the device

        :param metadata: The SiLA Client Metadata attached to the call

        """

    @abstractmethod
    def ConnectToDevice(self, ServerName: str, Timeout: float, *, metadata: MetadataDict) -> ConnectToDevice_Responses:
        """
        Makes the robot try to establish a connection to the specified device(this might take a few
            seconds). Doing this before the connection is actually needed may save time.


        :param ServerName: SiLA server name

        :param Timeout: timeout for creating a SilAclient

        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - Success: Boolean whether connection could be established


        """

    @abstractmethod
    def RemoveDevice(self, ServerName: str, *, metadata: MetadataDict) -> RemoveDevice_Responses:
        """
        Removes a device to the set.


        :param ServerName: The SilaService.server_name of the device

        :param metadata: The SiLA Client Metadata attached to the call

        """
