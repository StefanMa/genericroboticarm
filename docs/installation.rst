.. highlight:: shell

============
Installation
============


Stable release
--------------

To install genericroboticarm, run this command in your terminal:

.. code-block:: console

    $ pip install genericroboticarm

This is the preferred method to install genericroboticarm, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From source
-----------

