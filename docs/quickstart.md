# Quickstart Guide
1. Install the package as described on the main page

2. Start the software typing 

    ``` bash
   genericroboticarm -r Dummy
   ```
   The output should look similar to this:
   ```commandline
   Dash is running on http://127.0.0.1:8055/

   INFO| dash.run: Dash is running on http://127.0.0.1:8055/
   
   WARNING| sila_server.__start: Starting SiLA server without encryption. This violates the SiLA 2 specification.
    * Serving Flask app 'Dorna Movement Visualization'
      * Debug mode: off
   ```
3. You can now open http://127.0.0.1:8055/ in your browser. This is a simple dash interface, with an interactive 
   exemplary position graph and a text box in te upper left corner. While the focus (i.e. it was clicked last) is on it,
   you can control the robotic arm with the four arrow keys, 'a' and 'y' which will alter the current coordinates.
4. There is also a [SiLA2](https://sila-standard.com/standards/) server running in the background on 127.0.0.1:50051,
   you can access to give commands to the robot.
   You can do this either through f.e. python using [this guide](https://sila2.gitlab.io/sila_python/content/client/index.html)
   Or use another browser-tool (we recommend https://gitlab.com/unitelabs/sila2/sila-browser) which will automatically discover this server and enable point and click interaction.
5. With the dummy implementation you will see only some commandline outputs, change of current coordinates and the robots position in the graph after given commands (hint: The node labels are the position identifiers for positioning commands).
6. This should give an idea how a robot can be controlled using this tool box. If you want to apply it to your own robot follow the [adaption guide](/docs/adaption.md)