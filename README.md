# Generic Robotic Arm

A package containing utility and interfaces to teach and control a robotic arm.

## Features

- Enables control of a robotic arms individual joints by pressing keys
- Provides a SiLA2 server to give high level commands and teaching
- Navigation of the robotic arm through a taught position network
- Visualization of the robots position network and current path and location 
- Easy adaption to new hardware through clear interface
- Existing interface implementations for:
  - a simulated arm (Dummy)
  - an arduino controlled [JoyIt](https://joy-it.net/en/products/Robot02)
  - [Dorna2](https://dorna.ai/robots/dorna-2/)
  - [UFactory XArm](https://www.ufactory.cc/xarm-collaborative-robot/)

## Installation
 - For development:
   1. Clone the project using 
       ``` bash 
            git clone git@gitlab.com:StefanMa/genericroboticarm.git
       ```
   2. Install it using 
       ``` bash
            pip install -e genericroboticarm/.
       ```

## Usage
To learn how to start the software type 
``` bash
    genericroboticarm --help 
```
To learn how to use the software read the [Quick start guide](/docs/quickstart.md).

To adapt the software to your own robotic arm read the [Adapion Guide](/docs/adaption.md)


## Development

    git clone gitlab.com/StefanMa/genericroboticarm

    # create a virtual environment and activate it then run

    pip install -e .[dev]

    # run unittests

    invoke test   # use the invoke environment to manage development
    

## Documentation

The Documentation can be found here: [https://StefanMa.gitlab.io/genericroboticarm](https://StefanMa.gitlab.io/genericroboticarm) or [genericroboticarm.gitlab.io](genericroboticarm.gitlab.io/)


## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter)
 and the [gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage](https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage) project template.
## Contact:
    stefan.maak(at)uu(dot)se



